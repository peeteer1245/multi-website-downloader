import re
import os
import json
import config
import hashlib
import requests
import multiprocessing
import urllib.robotparser
from functools import partial


def get_site_text(website_url: str, robots_obj: urllib.robotparser.RobotFileParser):
    if robots_obj.can_fetch(url=website_url, useragent="*"):
        for extension in config.Config.FILE_EXTENSION_BLACKLIST:
            if website_url[len(website_url) - len(extension):].lower() in config.Config.FILE_EXTENSION_BLACKLIST:
                return ""
        for i in range(config.Config.TIMEOUT_RETRIES + 1):
            try:
                return requests.get(website_url).text
            except requests.exceptions.HTTPError:
                return ""
            except:
                pass
        return ""


def get_site_text_multithreaded(dl_threads: int, url_list: list, robots_obj):
    with multiprocessing.Pool(processes=dl_threads) as pool:
        results = pool.map(partial(get_site_text, robots_obj=robots_obj), url_list)
    return zip(url_list, results)


def find_all_urls(text: str):
    global base_domain
    absolute = re.findall("(https?:\/\/[\w\-\.\/\?\=\%]+)", text)
    relative = re.findall("href=\"(\/\S*)\">", text)
    no_proto = ["http://" + url for url in relative if url[:4] != "http" and url[:1] != "/"]
    everything_missing = [base_domain + url for url in relative if url[:1] == "/" and url[:2] != "//"]
    no_proto2 = ["http:" + url for url in relative if url[:4] != "http" and url[:2] == "//"]
    return absolute + no_proto + everything_missing


def get_url_base(url: str):
    return re.findall("(https?:\/\/[^/]+)", url)[0]


def get_total_found():
    global urls_collected
    counter = 0
    for key, value in urls_collected.items():
        counter += len(value)
    return counter


def get_total_checked():
    global progress_counter
    counter = 0
    for key, value in progress_counter.items():
        counter += value
    return counter


def write_file(text: str, save_dir: str):
    filehash = hashlib.sha256(text.encode("utf-8")).hexdigest()
    if not os.path.isdir("{}/{}".format(save_dir, filehash[:2])):
        os.mkdir("{}/{}".format(save_dir, filehash[:2]))
    if not os.path.isdir("{}/{}/{}".format(save_dir, filehash[:2], filehash[2:4])):
        os.mkdir("{}/{}/{}".format(save_dir, filehash[:2], filehash[2:4]))
    constructed_filename = save_dir + "/" + filehash[:2] + "/" + filehash[2:4] + "/" + filehash
    with open(constructed_filename + filehash, "w", encoding="utf-8") as f:
        f.write(text)


def url_contains_bad_extension(url: str):
    for extension in config.Config.FILE_EXTENSION_BLACKLIST:
        if url[len(url) - len(extension):] == extension:
            return True
    return False


if __name__ == '__main__':
    Config = config.Config("config.ini")

    with open(Config.WEBSITE_LIST_FILE, "r", encoding="utf-8") as f:
        master_list = f.readlines()

    master_list = [entry.replace("\n", "") for entry in master_list]

    if os.path.isfile(Config.FOUND_FILE) and os.path.isfile(Config.PROGRESS_FILE):
        with open(Config.FOUND_FILE, "r", encoding="utf-8") as f:
            urls_collected = json.load(f)

        if len(master_list) != len(urls_collected):
            for entry in master_list:
                urls_collected[entry] = [entry]

        with open(Config.PROGRESS_FILE, "r") as f:
            progress_counter = json.load(f)
            urls_checked = {}
            for key in urls_collected:
                try:
                    urls_checked[key] = urls_collected[key][:int(progress_counter[key])]
                except KeyError:
                    urls_checked[key] = urls_collected[key]
                    progress_counter[key] = 0
    else:
        urls_collected = {}
        urls_checked = {}
        progress_counter = {}

        for entry in master_list:
            urls_collected[entry] = [entry]
            urls_checked[entry] = []
            progress_counter[entry] = 0

    # preparations to save to disk
    if not os.path.isdir(Config.SAVE_FOLDER):
        os.mkdir(Config.SAVE_FOLDER)

    print("urls checked | urls found | completed in %")
    print("{} | {} |".format(get_total_checked(), get_total_found()))

    for website, entries in urls_collected.items():
        base_domain = get_url_base(website)
        if Config.RESPECT_ROBOTS_FILE:
            robots_address = base_domain + "/robots.txt"
        else:
            robots_address = "https://www.robotstxt.org/robots.txt"
        robots_txt_parser = urllib.robotparser.RobotFileParser(robots_address)
        robots_txt_parser.read()

        # more preparations to save to disk
        if Config.DUMP_EVERYTHING_IN_THE_SAME_FOLDER:
            save_dir = Config.SAVE_FOLDER
        else:
            if base_domain[:7] == "http://":
                sub_folder = base_domain[7:]
            elif base_domain[:8] == "https://":
                sub_folder = base_domain[8:]

            save_dir = Config.SAVE_FOLDER + "/" + sub_folder
            if not os.path.isdir(save_dir):
                os.mkdir(save_dir)

        while len(entries) != len(urls_checked[website]):
            # get a unvisited subset of found urls from url_collected
            url_download_list = []
            for url in entries:
                if url not in urls_checked[website]:
                    url_download_list.append(url)
                if len(url_download_list) >= Config.DOWNLOAD_THREADS:
                    break

            # download the subset
            new_sites_data = get_site_text_multithreaded(
                Config.DOWNLOAD_THREADS,
                url_download_list,
                robots_txt_parser
            )
            for checked_url, text in new_sites_data:
                new_urls = find_all_urls(text)
                for new_url in new_urls:
                    if base_domain in new_url \
                            and new_url not in urls_collected[website]\
                            and not url_contains_bad_extension(new_url):
                        urls_collected[website].append(new_url)
                urls_checked[website].append(checked_url)
                progress_counter[website] = len(urls_checked[website])
                write_file(text, save_dir)
            with open(Config.FOUND_FILE, "w") as f:
                json.dump(urls_collected, f)
            with open(Config.PROGRESS_FILE, "w") as f:
                json.dump(progress_counter, f)
            print("{} | {} | {}%".format(
                get_total_checked(),
                get_total_found(),
                str(get_total_checked() / get_total_found() * 100)[:5]))
