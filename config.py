import configparser


class Config:
    WEBSITE_LIST_FILE = "websites_todo.txt"
    FOUND_FILE = "found.txt"
    PROGRESS_FILE = "counter.txt"
    RESPECT_ROBOTS_FILE = True
    DUMP_EVERYTHING_IN_THE_SAME_FOLDER = True
    SAVE_FOLDER = "saved"
    DOWNLOAD_THREADS = 50
    FILE_EXTENSION_BLACKLIST = ".gif;.mp4;.webm;.webp;.jpg;.jpeg;.png;.css;.ogg;.mp3"
    TIMEOUT_RETRIES=0

    def __init__(self, configfile="config.ini"):
        config = configparser.ConfigParser()
        config.read(configfile)

        if "WEBSITE_LIST_FILE" in config["DEFAULT"]:
            Config.WEBSITE_LIST_FILE = config["DEFAULT"]["WEBSITE_LIST_FILE"]

        if "FOUND_FILE" in config["DEFAULT"]:
            Config.FOUND_FILE = config["DEFAULT"]["FOUND_FILE"]

        if "PROGRESS_FILE" in config["DEFAULT"]:
            Config.PROGRESS_FILE = config["DEFAULT"]["PROGRESS_FILE"]

        if "RESPECT_ROBOTS_FILE" in config["DEFAULT"]:
            if config["DEFAULT"]["RESPECT_ROBOTS_FILE"].lower() == "true":
                Config.RESPECT_ROBOTS_FILE = True
            else:
                Config.RESPECT_ROBOTS_FILE = False

        if "DUMP_EVERYTHING_IN_THE_SAME_FOLDER" in config["DEFAULT"]:
            if config["DEFAULT"]["DUMP_EVERYTHING_IN_THE_SAME_FOLDER"].lower() == "true":
                Config.DUMP_EVERYTHING_IN_THE_SAME_FOLDER = True
            else:
                Config.DUMP_EVERYTHING_IN_THE_SAME_FOLDER = False

        if "SAVE_FOLDER" in config["DEFAULT"]:
            Config.SAVE_FOLDER = config["DEFAULT"]["SAVE_FOLDER"]

        if "DOWNLOAD_THREADS" in config["DEFAULT"]:
            Config.DOWNLOAD_THREADS = int(config["DEFAULT"]["DOWNLOAD_THREADS"])

        if "FILE_EXTENSION_BLACKLIST" in config["DEFAULT"]:
            Config.FILE_EXTENSION_BLACKLIST = config["DEFAULT"]["FILE_EXTENSION_BLACKLIST"].lower().split(";")

        if "TIMEOUT_RETRIES" in config["DEFAULT"]:
            Config.TIMEOUT_RETRIES = int(config["DEFAULT"]["TIMEOUT_RETRIES"])
