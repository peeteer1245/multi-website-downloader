# Multi Website downloader
This is a purpose build application according to the rough specs listed on a reddit post [here](https://www.reddit.com/r/de_EDV/comments/if9eq1/bulk_download_von_websites/).

You supply a file with a list of websites and this script crawls every single of those websites, saving non-media-files to disk.

# Spec

Copy from reddit
```text
Ich habe eine Liste von 200.000 Websites.
Ich soll die Texte (und nur die Texte) der Websites herunterladen
und lokal als Textdatei (Hauptsache durchsuchbar) speichern.
Wobei ich darauf achten muss nur auf dieser Seite zu bleiben
(also keinen externen Links zu folgen, internen natürlich schon).
```

English summary
- I have a list of 200,000 Websites
- I want to save the text of all of those websites to local storage
- Stay on the domain

Implied
- Crawl through every of those Websites

# Usage
1. fill in the `config.ini` with your parameters
1. include all your unwanted file extensions in `config.ini` (case insensitive)
1. make sure the website master list is formatted properly (see [here](#website-master-list))
1. execute with `python3 main.py`

# directory structure
To avoid name duplication and file explorer crashes every file gets put into a folder corresponding to their file hash. The name of a file is the full sha256 hash of itself.  

A example `saved/gitlab.com/2a/fb/2afb....`.


# website master list
here is example content for the master list (it __must__ have a empty last line)
```text
https://www.reddit.com/r/de_EDV/comments/if9eq1/bulk_download_von_websites/
https://gitlab.com
...

```
